require("dotenv").config();
const express = require("express");
const jwt = require("jsonwebtoken");
const cors = require("cors");
const stripe = require("stripe")(process.env.SECRET_KEY);
const uuid = require("uuid").v4;
const PORT = 8088;
const app = express();
app.use(express.json());
app.use(cors());

//routes

app.post("/login", (req, res) => {
  const { username, password } = req.body;
  if (username && password) {
    const token = jwt.sign({ username, password }, process.env.MY_SECRET_KEY);
    console.log(token, "token");
    res.status(200).json({
      token: token,
      username,
    });
  } else {
    console.log(err);
    res.status(401).json({
      error: "error",
    });
  }
});

const ensureToken = (req, res, next) => {
  jwt.verify(req.body.authorization, process.env.MY_SECRET_KEY, (err, data) => {
    console.log(err, req.body.authorization);
    if (err) {
      res.sendStatus(403);
    } else {
      next();
    }
  });
};

app.get("/api/protected", ensureToken, (req, res) => {
  jwt.verify(req.token, process.env.MY_SECRET_KEY, (err, data) => {
    if (err) {
      res.sendStatus(403);
    } else {
      res.json({
        text: "this is protected",
      });
    }
  });
});

app.post("/payment", ensureToken, async (req, res) => {
  let status;
  let error;
  let data;
  try {
    const { product, token, address } = req.body;
    const idempotencyKey = uuid();
    // const resultData =
    await stripe.customers
      .create({
        email: token.email,
        source: token.id,
      })
      .then((customer) => {
        console.log(address, "address");
        stripe.charges
          .create(
            {
              amount: product.price,
              currency: "INR",
              customer: customer.id,
              receipt_email: token.email,
              description: product.name,
              shipping: {
                name: token.card.name,
                address: {
                  ...address,
                },
              },
            },
            { idempotencyKey }
          )
          .then((result) => {
            res.status(200).json({
              statusCode: 200,
              data: result,
            });
          })
          .catch((err) => {
            console.log(err, "error");
            res.status(400).json({
              statusCode: 400,
              data: [],
              error: err,
            });
          });
      });
    // console.log("CHARGE", resultData)
    // status = 'success'
    // data = resultData
    // res.send(resultData)
  } catch (err) {
    console.log("ERR", err);
    status = "fail";
    res.send(err);
  }

  // res.json({error, status, data})
});
app.get("/", (req, res) => {
  res.send("ok");
});

//listen

app.listen(PORT, () => {
  console.log(`App is running on port ${PORT}`);
});
